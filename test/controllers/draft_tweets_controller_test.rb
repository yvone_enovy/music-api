require 'test_helper'

class DraftTweetsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @draft_tweet = draft_tweets(:one)
  end

  test "should get index" do
    get draft_tweets_url, as: :json
    assert_response :success
  end

  test "should create draft_tweet" do
    assert_difference('DraftTweet.count') do
      post draft_tweets_url, params: { draft_tweet: { avatar: @draft_tweet.avatar, description: @draft_tweet.description, user_name: @draft_tweet.user_name } }, as: :json
    end

    assert_response 201
  end

  test "should show draft_tweet" do
    get draft_tweet_url(@draft_tweet), as: :json
    assert_response :success
  end

  test "should update draft_tweet" do
    patch draft_tweet_url(@draft_tweet), params: { draft_tweet: { avatar: @draft_tweet.avatar, description: @draft_tweet.description, user_name: @draft_tweet.user_name } }, as: :json
    assert_response 200
  end

  test "should destroy draft_tweet" do
    assert_difference('DraftTweet.count', -1) do
      delete draft_tweet_url(@draft_tweet), as: :json
    end

    assert_response 204
  end
end
