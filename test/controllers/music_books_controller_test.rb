require 'test_helper'

class MusicBooksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @music_book = music_books(:one)
  end

  test "should get index" do
    get music_books_url, as: :json
    assert_response :success
  end

  test "should create music_book" do
    assert_difference('MusicBook.count') do
      post music_books_url, params: { music_book: { author_id: @music_book.author_id, img_url: @music_book.img_url, price: @music_book.price, title: @music_book.title } }, as: :json
    end

    assert_response 201
  end

  test "should show music_book" do
    get music_book_url(@music_book), as: :json
    assert_response :success
  end

  test "should update music_book" do
    patch music_book_url(@music_book), params: { music_book: { author_id: @music_book.author_id, img_url: @music_book.img_url, price: @music_book.price, title: @music_book.title } }, as: :json
    assert_response 200
  end

  test "should destroy music_book" do
    assert_difference('MusicBook.count', -1) do
      delete music_book_url(@music_book), as: :json
    end

    assert_response 204
  end
end
