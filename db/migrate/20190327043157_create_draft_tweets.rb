class CreateDraftTweets < ActiveRecord::Migration[5.2]
  def change
    create_table :draft_tweets do |t|
      t.string :user_name
      t.string :avatar
      t.text :description

      t.timestamps
    end
  end
end
