class CreateMusicBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :music_books do |t|
      t.string :title
      t.integer :author_id
      t.decimal :price
      t.string :img_url

      t.timestamps
    end
  end
end
