Rails.application.routes.draw do
  resources :draft_tweets
  resources :tweets
  resources :users
  resources :authors
  resources :music_books
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
