class DraftTweetsController < ApplicationController
  before_action :set_draft_tweet, only: [:show, :update, :destroy]

  # GET /draft_tweets
  def index
    @draft_tweets = DraftTweet.all

    render json: {draft_tweets: @draft_tweets}
  end

  # GET /draft_tweets/1
  def show
    render json: {draft_tweet: @draft_tweet}
  end

  # POST /draft_tweets
  def create
    @draft_tweet = DraftTweet.new(draft_tweet_params)

    if @draft_tweet.save
      render json: {draft_tweet: @draft_tweet}, status: :created, location: @draft_tweet
    else
      render json: @draft_tweet.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /draft_tweets/1
  def update
    if @draft_tweet.update(draft_tweet_params)
      render json: {draft_tweet: @draft_tweet}
    else
      render json: @draft_tweet.errors, status: :unprocessable_entity
    end
  end

  # DELETE /draft_tweets/1
  def destroy
    @draft_tweet.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_draft_tweet
      @draft_tweet = DraftTweet.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def draft_tweet_params
      params.require(:draft_tweet).permit(:user_name, :avatar, :description)
    end
end
