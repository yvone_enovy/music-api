class MusicBooksController < ApplicationController
  before_action :set_music_book, only: [:show, :update, :destroy]

  # GET /music_books
  def index
    @music_books = MusicBook.all

    render json: {music_books: @music_books}
  end

  # GET /music_books/1
  def show
    render json: {music_book: @music_book}
  end

  # POST /music_books
  def create
    @music_book = MusicBook.new(music_book_params)

    if @music_book.save
      render json: {music_book: @music_book}, status: :created, location: @music_book
    else
      render json: @music_book.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /music_books/1
  def update
    if @music_book.update(music_book_params)
      render json: {music_book: @music_book}
    else
      render json: @music_book.errors, status: :unprocessable_entity
    end
  end

  # DELETE /music_books/1
  def destroy
    @music_book.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_music_book
      @music_book = MusicBook.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def music_book_params
      params.require(:music_book).permit(:title, :author_id, :price, :img_url)
    end
end
